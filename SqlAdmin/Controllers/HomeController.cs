﻿using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
//using Vikas.Generics.Data;

namespace SqlAdmin.Controllers
{
    public class SqlModel
    {
        public String ConnectionString { get; set; }
        public String Query { get; set; }
    }
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Execute(SqlModel model)
        {
            Backup();
            System.Data.DataSet DS = null;
            if (model != null && !string.IsNullOrEmpty(model.Query))
            {
                try
                {
                    var DB = Vikas.Generics.Data.DatabaseFactory.GetDatabase();
                    DS = await DB.GetDataSet(model.Query);
                }
                catch (Exception ex)
                {
                    ViewBag.Error = ex.Message;
                    ViewBag.Trace = "trace- " + ex.StackTrace;
                }
            }

            return PartialView("_Grid", DS);
        }

        public async Task Backup()
        {
            var DB = Vikas.Generics.Data.DatabaseFactory.GetDatabase();
            var dt = await DB.GetDataTable("select * from sys.tables;");
            var tables = new List<string>();
            foreach (DataRow item in dt.Rows)
            {
                tables.Add(item["name"].ToString());
            }
            dt = await DB.GetDataTable("SELECT * FROM sys.objects WHERE type_desc IN('SQL_INLINE_TABLE_VALUED_FUNCTION','SQL_SCALAR_FUNCTION');");
            var Func = new List<string>();
            foreach (DataRow item in dt.Rows)
            {
                Func.Add(item["name"].ToString());
            }
            dt = await DB.GetDataTable("select * from sys.procedures;");
            var SPs = new List<string>();
            foreach (DataRow item in dt.Rows)
            {
                SPs.Add(item["name"].ToString());
            }
            string FileName = "b:\\abc.sql";
            //string[] Tables=new st;
            StringBuilder sb = new StringBuilder();
            Server srv = new Server(new ServerConnection("209.99.16.18", "Rahul", "Rahul@123"));
            Database dbs = srv.Databases["RahulDB"];
            ScriptingOptions options = new ScriptingOptions();
            options.ScriptData = true;
            //options.WithDependencies = true; used to get dependency tables...
            options.ScriptDrops = false;
            options.FileName = FileName;
            options.EnforceScriptingOptions = true;
            options.ScriptSchema = true;
            options.IncludeHeaders = true;
            options.AppendToFile = true;
            options.Indexes = true;
            // foreach (var tbl in Tables)
            // {
            try
            {
                var i = 0;
                foreach (Table item in dbs.Tables)
                {
                    if(tables.Any(x=>x== item.Name)){
                        dbs.Tables[i].EnumScript(options);
                    }
                    i++;
                }

                options.ScriptData = false;
                i = 0;
                foreach (UserDefinedFunction item in dbs.UserDefinedFunctions)
                {
                    if (Func.Any(x => x == item.Name))
                    {
                        dbs.UserDefinedFunctions[i].Script(options);
                    }
                    i++;
                   // dbs.UserDefinedFunctions[i++].Script(options);
                }

                i = 0;
                foreach (StoredProcedure item in dbs.StoredProcedures)
                {
                    if (SPs.Any(x => x == item.Name))
                    {
                        dbs.StoredProcedures[i].Script(options);
                    }
                    i++;
                    //dbs.StoredProcedures[i++].Script(options);
                }
                var qq = dbs.Tables[0].EnumScript(options);
                //var ee = dbs.Script(options);
            }
            catch (Exception ex)
            {
                var a = ex.Message;
            }

            // }
        }
    }
}
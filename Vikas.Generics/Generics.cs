﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;

namespace Vikas.Generics
{
    public static class Generics
    {
        public static Dictionary<TType, TValue> ToDictionary<TType, TValue>(this object obj)
        {
            var json = JsonConvert.SerializeObject(obj);
            var dictionary = JsonConvert.DeserializeObject<Dictionary<TType, TValue>>(json);
            return dictionary;
        }
        public static DataTable ToTable<T>(this IEnumerable<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
        public static DataTable Fill<T>(this DataTable table, IEnumerable<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    if (row.Table.Columns.Contains(prop.Name))
                        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                }
                table.Rows.Add(row);
            }
            return table;
        }

        public static List<T> ToList<T>(this DataTable table) where T : class, new()
        {
            try
            {
                List<T> list = new List<T>();

                foreach (var row in table.AsEnumerable())
                {
                    T obj = new T();

                    foreach (var prop in obj.GetType().GetProperties())
                    {
                        PropertyInfo propertyInfo = null;
                        try
                        {
                            propertyInfo = obj.GetType().GetProperty(prop.Name);
                            if (propertyInfo.PropertyType.IsEnum)
                            {

                                propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], Enum.GetUnderlyingType(propertyInfo.PropertyType)), null);
                            }
                            else
                            {
                                propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                            }
                        }
                        catch (InvalidCastException ex)
                        {
                            if (propertyInfo != null && row[prop.Name] != DBNull.Value && !propertyInfo.PropertyType.IsEnum)
                            {
                                try
                                {
                                    propertyInfo.SetValue(obj, row[prop.Name], null);
                                }
                                catch { }
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }

                    list.Add(obj);
                }

                return list;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static T ToObject<T>(this DataTable table) where T : class, new()
        {
            try
            {
                T Obj = new T();

                foreach (var row in table.AsEnumerable())
                {

                    foreach (var prop in Obj.GetType().GetProperties())
                    {
                        PropertyInfo propertyInfo = null;
                        try
                        {
                            propertyInfo = Obj.GetType().GetProperty(prop.Name);
                            if (propertyInfo.PropertyType.IsEnum)
                            {

                                propertyInfo.SetValue(Obj, Convert.ChangeType(row[prop.Name], Enum.GetUnderlyingType(propertyInfo.PropertyType)), null);
                            }
                            else
                            {
                                propertyInfo.SetValue(Obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                            }
                        }
                        catch (InvalidCastException ex)
                        {
                            if (propertyInfo != null && row[prop.Name] != DBNull.Value && !propertyInfo.PropertyType.IsEnum)
                            {
                                try
                                {
                                    propertyInfo.SetValue(Obj, row[prop.Name], null);
                                }
                                catch { }
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }

                    break;
                }

                return Obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static List<T> ToListByJson<T>(this DataTable table) where T : class, new()
        {
            try
            {
                var JsonTable = JsonConvert.SerializeObject(table);
                var Data = JsonConvert.DeserializeObject<List<T>>(JsonTable, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
                return Data;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static T ToObjectByJson<T>(this DataTable table) where T : class, new()
        {
            try
            {
                var JsonTable = JsonConvert.SerializeObject(table);
                var Data = JsonConvert.DeserializeObject<List<T>>(JsonTable, new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
                return Data != null && Data.Count > 0 ? Data[0] : new T();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static string GetHash(this string input)
        {
            HashAlgorithm hashAlgorithm = new SHA256CryptoServiceProvider();
            byte[] byteValue = System.Text.Encoding.UTF8.GetBytes(input);
            byte[] byteHash = hashAlgorithm.ComputeHash(byteValue);
            return Convert.ToBase64String(byteHash);
        }
        public static T ToEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
        public static byte[] GetByteArreyfromBase64Img(this string imgBase64)
        {
            return Convert.FromBase64String(imgBase64.Split(',')[1]);
        }
        public static string GetImgBase64FromByteArrey(this byte[] imgBytes)
        {
            return "data:image/png;base64," + Convert.ToBase64String(imgBytes);
        }

        public static bool HasProperty(dynamic obj, string property)
        {
            return ((Type)obj.GetType()).GetProperties().Where(p => p.Name.Equals(property)).Any();
        }
        public static IEnumerable<dynamic> ListFromTable(this DataTable table)
        {
            if (table == null)
            {
                yield break;
            }

            foreach (DataRow row in table.Rows)
            {
                IDictionary<string, object> dRow = new ExpandoObject();

                foreach (DataColumn column in table.Columns)
                {
                    var value = row[column.ColumnName];
                    dRow[column.ColumnName] = Convert.IsDBNull(value) ? null : value;
                }

                yield return dRow;
            }
        }
        public static List<dynamic> ToList(this DataTable table)
        {
            IEnumerable<dynamic> lst = table.AsEnumerable().Select(row => new DynamicRow(row)).ToList();
            return lst.ToList();
        }
        public sealed class DynamicRow : DynamicObject
        {
            private readonly DataRow _row;

            public DynamicRow(DataRow row) { _row = row; }
            // Interprets a member-access as an indexer-access on the 
            // contained DataRow.
            public override bool TryGetMember(GetMemberBinder binder, out object result)
            {
                var retVal = _row.Table.Columns.Contains(binder.Name) && _row[binder.Name] != DBNull.Value;
                result = retVal ? _row[binder.Name] : null;
                return true;
            }
        }

    }
}

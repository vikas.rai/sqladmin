﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vikas.Generics.Data
{
    public class ConnectionSetting
    {
        public string ConnectionString { get; set; }
        public string ProviderName { get; set; }
        public string ParameterPrefix { get; set; }
    }

    public class DatabaseFactory
    {
        private static Database _database { get; set; }
        public static ConnectionStringSettings ConnectionStringSettings { get; set; }
        public static string ParameterPrefix { get; set; }


        public static Database GetDatabase()
        {
            if (_database == null)
            {
                if (ConnectionStringSettings == null)
                {
                    throw new Exception("Please initilize connectionstrig.");
                }
                _database = new Database(ConnectionStringSettings, ParameterPrefix);
            }
            return _database;
        }
        public static Database GetDatabase(ConnectionStringSettings _connectionSetting, string _parameterPrefix = null)
        {
            if (_connectionSetting == null)
            {
                throw new Exception("Please initilize connectionstrig");
            }
            return new Database(_connectionSetting, _parameterPrefix);
        }
    }
}

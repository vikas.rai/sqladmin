﻿using System;

namespace Vikas.Generics.NetCore
{
    public static class DateTimeHelper
    {
        public static readonly DateTime MinValue = new DateTime(1970, 1, 1);
        public static double ToUnix(this DateTime d)
        {
            return (d.Subtract(MinValue)).TotalSeconds;
        }
        public static DateTime FromUnix(this double d)
        {
            return MinValue.AddSeconds(d);
        }
        public static TimeSpan Diff(this DateTime StartDate, DateTime EndDate)
        {
            return StartDate == DateTime.MinValue || EndDate == DateTime.MinValue ? new TimeSpan(0, 0, 0, 0) : EndDate - StartDate;
        }
    }
}
